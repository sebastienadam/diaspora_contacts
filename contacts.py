#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Made by Hārokku https://framasphere.org/people/04385b502a77013283742a0000053625

import argparse
import json

def GetArgs():
    """ Retrieve command line parametrers """
    parser = argparse.ArgumentParser(description='Recovers contacts from a diaspora* export file')
    parser.add_argument('-i', '--inputfile', help='Configuration ID that must be reactivated', required=True)
    parser.add_argument('-o', '--ouputfile', help='Configuration ID that must be disabled', required=False, default='contacts.txt')
    return vars(parser.parse_args())

def Main():
    """ Entry script """
    args = GetArgs()
    with open(args['inputfile'], encoding='utf8') as fin:
        data = json.load(fin)
        with open(args['ouputfile'], 'w', encoding='utf8') as fout:
            contacts = {}
            for contact in data['user']['contacts']:
                fout.write('{:<48}-> {}\n'.format(contact['account_id'],', '.join(contact['contact_groups_membership'])))

if __name__ == '__main__':
    Main()
