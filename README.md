# Diaspora* contacts

The purpose of this script is to retrieve contacts from a [diaspora*](https://diasporafoundation.org/) export file. It can be run using [Python 3](https://www.python.org/).

## Usage

First download your profile data. Then run the script as follow:

````
$ contacts.py -i <profile_export_file> [-o <output_file>]
````

`<profile_export_file>` should be replaced by the name of the file must be replaced by the name of the file containing the data from your diaspora* profile that you uploaded. You can optionally give an output file name using the `-o` option. If you do not use this option, the data will be saved in a file called `contacts.txt`.
